﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Battery : MonoBehaviour {
    public GameObject PlayerGameObject;
    private PlayerController _playerController;
    public float Power = 50;
    private bool _isPlayerInRange;
    private bool _isPickedUp;
    public Collider2D BatteryCollider;
    private Vector3 _originalScale;
    private bool _hasBeenUsed;

    // Use this for initialization
	void Start () {
        _playerController = PlayerGameObject.GetComponent<PlayerController>();
	    _originalScale = transform.localScale;
    }

    // Update is called once per frame
    void Update ()
    {
        if(_hasBeenUsed)
        {
            return;
        }
	    if (Input.GetKeyDown(KeyCode.E) && _isPlayerInRange)
	    {
            _playerController.AddPower(Power);
            _hasBeenUsed = true;
            Destroy(gameObject);
	    }

	    if (Input.GetKeyDown(KeyCode.Space) && _isPlayerInRange)
	    {
	        _isPickedUp = !_isPickedUp;
	        BatteryCollider.enabled = !_isPickedUp;
	    }

        if (_isPickedUp)
	    {
	        transform.position = PlayerGameObject.transform.position;
	        transform.localScale = new Vector3(_originalScale.x / 2, _originalScale.y / 2);
	    }
	    else
	    {
	        transform.localScale = _originalScale;
	    }
    }

    public void OnTriggerEnter2D(Collider2D other)
    {
        if (other.gameObject.tag == "Player")
        {
            _isPlayerInRange = true;
        }
    }

    public void OnTriggerExit2D(Collider2D other)
    {
        if (other.gameObject.tag == "Player")
        {
            _isPlayerInRange = false;
        }
    }
}
