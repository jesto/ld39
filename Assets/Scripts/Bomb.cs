﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Bomb : MonoBehaviour {

    private bool _isPlayerInRange;
    private Animator _animator;
    public GameObject PlayerGameObject;
    private int _explodeStateHashId;
    private int _explodedStateHashId;
    private bool _isPickedUp;
    public CapsuleCollider2D BombCollider;
    private List<GameObject> _gameObjectsInRange;
    private Vector3 _originalScale;
    private bool _hasExploded;

    // Use this for initialization
    void Start () {
        _gameObjectsInRange = new List<GameObject>();
	    _animator = GetComponentInParent<Animator>();
	    _explodeStateHashId = Animator.StringToHash("Base.Explode");
	    _explodedStateHashId = Animator.StringToHash("Base.Exploded");
        _originalScale = transform.localScale;
    }

    // Update is called once per frame
    void Update () {
        if (PlayerGameObject == null || _hasExploded)
        {
            return;
        }

        if (Input.GetKeyDown(KeyCode.E) && _isPlayerInRange)
	    {
            _animator.SetTrigger("StartCountdown");
        }

        if (Input.GetKeyDown(KeyCode.Space) && _isPlayerInRange)
	    {
            _isPickedUp = !_isPickedUp;
            BombCollider.enabled = !_isPickedUp;
        }

        if(_isPickedUp)
        {
            transform.position = PlayerGameObject.transform.position;
            transform.localScale = new Vector3(_originalScale.x / 2, _originalScale.y / 2);
        }
        else
        {
            transform.localScale = _originalScale;
        }

        if (_animator.GetCurrentAnimatorStateInfo(0).fullPathHash == _explodeStateHashId)
        {
            transform.localScale = new Vector3(_originalScale.x * 2.5f, _originalScale.y * 2.5f);
            var objectsToBlowUp = new List<GameObject>(_gameObjectsInRange);
            foreach (var gameObjectToBlowUp in objectsToBlowUp)
            {
                var blowable = gameObjectToBlowUp.GetComponent<Blowable>();

                if (blowable != null)
                {
                    blowable.BlowUp();
                }
            }
        }

        if (_animator.GetCurrentAnimatorStateInfo(0).fullPathHash == _explodedStateHashId)
        {
            Destroy(gameObject);
            _hasExploded = true;
        }
    }

    public void OnTriggerEnter2D(Collider2D other)
    {
        _gameObjectsInRange.Add(other.gameObject);

        if (other.gameObject.tag == "Player")
        {
            _isPlayerInRange = true;
        }
    }

    public void OnTriggerExit2D(Collider2D other)
    {
        _gameObjectsInRange.Remove(other.gameObject);

        if (other.gameObject.tag == "Player")
        {
            _isPlayerInRange = false;
        }
    }
}
