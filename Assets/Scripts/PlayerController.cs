﻿using System;
using UnityEngine;

public class PlayerController : Blowable
{
	public int Healh = 1;
	public float PowerRemaining = 1;
	public float MaxPowerCapacity = 300;
	public float PowerUsageIdle = 1;
	public float PowerUsageMoving = 1;
	public float PowerUsageShooting = 1;
	public float WalkSpeed = 1;
    public GameObject GameManager;
	private Rigidbody2D _rigidbody2D;
	private float _horizontalSpeed;
	private float _verticalSpeed;
	public GameObject Bullet;
	public float ShootingDelay = 0;
	private float TimeSinceLastShot = 0;
    private Animator _animator;
    private GameManager _gameManager;

	public Action<int> OnDamageTaken;

    // Use this for initialization
	void Start () {
		_rigidbody2D = GetComponent<Rigidbody2D>();
        _animator = GetComponent<Animator>();
	    _gameManager = GameManager.GetComponent<GameManager>();

	    _gameManager.PushNarration("Initializing system");
	    _gameManager.PushNarration("Running system diagnostics");
	    _gameManager.PushNarration("ERROR - Memory corruption detected");
	    _gameManager.PushNarration("System ready - Current assignment unknown");
	    _gameManager.PushNarration("Well... I better find something to do");
    }

    void FixedUpdate()
	{
		var vector = new Vector2(_horizontalSpeed + transform.position.x, _verticalSpeed + transform.position.y);
		_rigidbody2D.MovePosition(vector);
	}

    // Update is called once per frame
    void Update ()
    {
        if (_gameManager.IsGameOver())
        {
            return;
        }
        
		PowerRemaining -= Time.deltaTime * PowerUsageIdle;

	    if (PowerRemaining <= 0)
	    {
	        _gameManager.LostAllPower();
	        Destroy(gameObject);
        }

        Move();
		Shoot();
	}

    private void Move()
	{
	    var horizontalAxis = Input.GetAxis("Horizontal");
	    var verticalAxis = Input.GetAxis("Vertical");
	    _horizontalSpeed = horizontalAxis != 0 ? horizontalAxis / 10 * WalkSpeed : 0;
	    _verticalSpeed = verticalAxis != 0 ? verticalAxis / 10 * WalkSpeed : 0;

		if (_horizontalSpeed != 0 || _verticalSpeed != 0)
			PowerRemaining -= Time.deltaTime * PowerUsageMoving;

	    _animator.SetBool("MovingLeft", horizontalAxis < 0);
	    _animator.SetBool("MovingRight", horizontalAxis > 0);
	    _animator.SetBool("MovingDown", verticalAxis < 0);
	    _animator.SetBool("MovingUp", verticalAxis > 0);
	}

    void Shoot()
	{
	    TimeSinceLastShot += Time.deltaTime;

		if (TimeSinceLastShot < ShootingDelay)
			return;

		var fireVertical = Input.GetAxis("FireVertical");
		var fireHorizontal = Input.GetAxis("FireHorizontal");
		if (fireVertical != 0 || fireHorizontal != 0)
		{
		    var safetyOffset = 0.08f;

            float bulletOffsetX = 0;
		    if(fireHorizontal != 0)
		    {
		        bulletOffsetX = fireHorizontal > 0 ? Bullet.transform.localScale.x + safetyOffset : -Bullet.transform.localScale.x - safetyOffset;
		    }

            float bulletOffsetY = 0;
            if (fireVertical != 0)
            {
                bulletOffsetY = fireVertical > 0 ? Bullet.transform.localScale.y + safetyOffset : -Bullet.transform.localScale.y - safetyOffset;
            }

		    var spawnPosition = new Vector3(gameObject.transform.position.x + ((fireHorizontal*transform.localScale.x) + bulletOffsetX) * 0.5f, gameObject.transform.position.y + ((fireVertical*transform.localScale.y) + bulletOffsetY) * 0.5f, 0);
            var bulletGameObject = Instantiate(Bullet, spawnPosition, Quaternion.identity);
			bulletGameObject.GetComponent<BulletMovement>().SetVelocity(new Vector2(fireHorizontal, fireVertical));

			PowerRemaining -= PowerUsageShooting;

			TimeSinceLastShot = 0;
        }
	}

    public void Hit(int dmg)
    {
        Healh -= dmg;

        _gameManager.PushNarration("Ouch, that hurt");
        
        if (Healh <= 0)
        {
            _gameManager.LostAllLife();
            Destroy(gameObject);
        }

	    if (OnDamageTaken != null)
	    {
	        OnDamageTaken(Healh);
	    }
    }

    public override void BlowUp()
    {
        Hit(1000);
    }

    public void AddPower(float power)
    {
        if (PowerRemaining >= MaxPowerCapacity)
        {
            _gameManager.PushNarration("If i drink any more I won't be able to keep it down");
            return;
        }

        PowerRemaining += power;

        _gameManager.PushNarration("*GULP* - Ahh... so nice");
    }
}
