﻿using UnityEngine;

public class AutoDoor : Blowable {
    private Animator _animator;
    public BoxCollider2D DoorCollider;
    private int _openStateHashId;
    private Collider2D _latestObjectInsideTriggerArea;
    private bool _isBlowedUp;
    private int _destroyedStateHashId;
    public bool RequiresKeyCard;
    private bool _isKeycardInRange;

    // Use this for initialization
	void Start ()
	{
	    _animator = GetComponent<Animator>();
	    _openStateHashId = Animator.StringToHash("Base.Open");
	    _destroyedStateHashId = Animator.StringToHash("Base.Destroyed");
    }
	
    // Update is called once per frame
    void Update ()
	{
	    var fullPathHash = _animator.GetCurrentAnimatorStateInfo(0).fullPathHash;
	    DoorCollider.enabled = fullPathHash != _openStateHashId && fullPathHash != _destroyedStateHashId;
	}

    public void OnTriggerEnter2D(Collider2D other)
    {
        if (_isBlowedUp)
        {
            return;
        }

        if (other.gameObject.tag == "KeyCard")
        {
            _isKeycardInRange = true;
        }

        TryToOpenDoor(other);
    }

    public void OnTriggerExit2D(Collider2D other)
    {
        if (_isBlowedUp)
        {
            return;
        }

        if (_latestObjectInsideTriggerArea == other && (other.gameObject.tag == "Enemy" || other.gameObject.tag == "Player" || other.gameObject.tag == "Bomb"))
        {
            _animator.SetBool("IsClosed", true);
        }

        if (other.gameObject.tag == "KeyCard")
        {
            _isKeycardInRange = false;
        }
    }

    public void OnTriggerStay2D(Collider2D other)
    {
        if (_isBlowedUp || other.gameObject.tag == "KeyCard")
        {
            return;
        }

        _latestObjectInsideTriggerArea = other;

        TryToOpenDoor(other);
    }

    private void TryToOpenDoor(Collider2D other)
    {
        if (RequiresKeyCard && !_isKeycardInRange)
        {
            return;
        }

        if (other.gameObject.tag == "Enemy" || other.gameObject.tag == "Player")
        {
            _animator.SetBool("IsClosed", false);
        }
    }

    public override void BlowUp()
    {
        _animator.SetBool("IsDestroyed", true);
        _isBlowedUp = true;
    }

    public bool IsClosed()
    {
        return DoorCollider.enabled;
    }
}