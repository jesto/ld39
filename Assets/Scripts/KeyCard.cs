﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class KeyCard : MonoBehaviour {
    private bool _isPlayerInRange;
    private bool _isPickedUp;
    public Collider2D KeyCardCollider;
    private GameObject _playerGameObject;
    private Vector3 _originalScale;


    // Use this for initialization
    void Start () {
        _playerGameObject = GameObject.FindWithTag("Player");
        _originalScale = transform.localScale;
    }
	
	// Update is called once per frame
	void Update () {
	    if (Input.GetKeyDown(KeyCode.Space) && _isPlayerInRange)
	    {
	        _isPickedUp = !_isPickedUp;
	        KeyCardCollider.enabled = !_isPickedUp;
	    }

	    if (_isPickedUp)
	    {
	        transform.position = _playerGameObject.transform.position;
	        transform.localScale = new Vector3(_originalScale.x / 2, _originalScale.y / 2);
	    }
	    else
	    {
	        transform.localScale = _originalScale;
	    }
    }

    public void OnTriggerEnter2D(Collider2D other)
    {
        if (other.gameObject.tag == "Player")
        {
            _isPlayerInRange = true;
        }
    }

    public void OnTriggerExit2D(Collider2D other)
    {
        if (other.gameObject.tag == "Player")
        {
            _isPlayerInRange = false;
        }
    }
}
