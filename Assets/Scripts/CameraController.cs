﻿using UnityEngine;

public class CameraController : MonoBehaviour
{
	public GameObject Player;

	void Update ()
	{
		if (Player == null) return;

		transform.position = new Vector3(Player.transform.position.x, Player.transform.position.y, transform.position.z);
	}
}
