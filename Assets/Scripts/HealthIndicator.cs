﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HealthIndicator : MonoBehaviour
{
	public GameObject FullHeartPrefab;
	public GameObject EmptyHeartPrefab;
	private GameObject[] Health = new GameObject[3];

	void Start()
	{
		UpdateHealth(Health.Length);
	}


	public void UpdateHealth(int health)
	{
		var offset = 0;
		for (int i = 0; i < Health.Length; i++, offset -= 35)
		{
			if(Health[i] != null)
				Destroy(Health[i]);

			if (i < health)
				Health[i] = Instantiate(FullHeartPrefab, transform);
			else
				Health[i] = Instantiate(EmptyHeartPrefab, transform);

			Health[i].transform.position = new Vector3(Health[i].transform.position.x + offset, Health[i].transform.position.y, Health[i].transform.position.z);
		}
	}
}
