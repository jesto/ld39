﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class BatteryIndicator : MonoBehaviour
{
	public GameObject ImageGameObject;
	public GameObject PowerRemainingTextGameObject;

	private Text _powerRemainingText;
	public float MaxBatteryLevel = 200;
	private RectTransform Image;

	void Start()
	{
		Image = ImageGameObject.GetComponent<RectTransform>();
		_powerRemainingText = PowerRemainingTextGameObject.GetComponent<Text>();
	}

	public void SetBattery(float wh)
	{
		var wWidth = 475f / MaxBatteryLevel;

		Image.SetSizeWithCurrentAnchors(RectTransform.Axis.Horizontal, wWidth * wh);
		_powerRemainingText.text = string.Format("{0:F2} Wh", wh);
	}
}
