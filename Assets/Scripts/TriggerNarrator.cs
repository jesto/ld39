﻿using UnityEngine;

public class TriggerNarrator : MonoBehaviour
{
	public string Text = "";
	private GameManager _gameManager;

	void Start ()
	{
		_gameManager = FindObjectOfType<GameManager>();
	}

    public void OnTriggerEnter2D(Collider2D col)
    {
        if (col.gameObject.tag == "Player")
        {
            _gameManager.PushNarration(Text);
            gameObject.SetActive(false);
        }
    }
}
