﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Terminal : MonoBehaviour {
    private bool _isPlayerInRange;
    private bool _hasBeenActivated;
    public GameObject GameManager;
    private GameManager _gameManager;
    private Animator _animator;

    // Use this for initialization
    void Start () {
        _gameManager = GameManager.GetComponent<GameManager>();
        _animator = GetComponent<Animator>();
    }

    // Update is called once per frame
    void Update () {
	    if (!_hasBeenActivated && Input.GetKeyDown(KeyCode.E) && _isPlayerInRange)
	    {
            _hasBeenActivated = true;
            _animator.SetTrigger("IsActivatedByPlayer");
        }

        if (Input.GetKeyDown(KeyCode.Y) && _isPlayerInRange)
	    {
	        _animator.SetTrigger("IsInitialized");
	        _gameManager.DestructionInitialized();
            _hasBeenActivated = false;
        }
        else if (Input.GetKeyDown(KeyCode.N) && _isPlayerInRange)
        {
            _animator.SetTrigger("IsAborted");
            _gameManager.DestructionAborted();
            _hasBeenActivated = false;
        }
    }

    public void OnTriggerEnter2D(Collider2D other)
    {
        if (other.gameObject.tag == "Player")
        {
            _isPlayerInRange = true;
        }
    }

    public void OnTriggerExit2D(Collider2D other)
    {
        if (other.gameObject.tag == "Player")
        {
            _isPlayerInRange = false;
        }
    }
}
