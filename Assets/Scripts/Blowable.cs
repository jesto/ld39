﻿using UnityEngine;

public abstract class Blowable : MonoBehaviour
{
    public abstract void BlowUp();
}