﻿using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class GameManager : MonoBehaviour
{
	public GameObject PlayerGameObject;
	
	public GameObject HealthCanvasGameObject;
	public Narrator Narrator;
	public GameObject NarratorTextGameObject;
	public GameObject GameOverPanelGameObject;
	public GameObject BatteryIndicatorGameObject;
	public GameObject HUDGameObject;
	private PlayerController _playerController;
	
    private bool _isGameOver;
	private BatteryIndicator _batteryIndicator;
    private Text _gameOverText;

	// Use this for initialization
	void Start ()
	{
		_playerController = PlayerGameObject.GetComponent<PlayerController>();
		
		var healthUI = HealthCanvasGameObject.GetComponent<HealthIndicator>();
		_playerController.OnDamageTaken += health => healthUI.UpdateHealth(health);
        _gameOverText = GameOverPanelGameObject.GetComponentInChildren<Text>();
		_batteryIndicator = BatteryIndicatorGameObject.GetComponent<BatteryIndicator>();
    }

    // Update is called once per frame
    void Update ()
	{
        if (!_isGameOver)
	    {
	        GameOverPanelGameObject.SetActive(false);
        }

        if(_isGameOver && Input.GetKeyDown(KeyCode.R))
        {
            SceneManager.LoadScene("SCENE0");
        }

        if (Input.GetKeyDown(KeyCode.Escape))
		{
			Application.Quit();
		}

		_batteryIndicator.SetBattery(_playerController.PowerRemaining);
	}

	public void LostAllLife()
	{
	    ShowGameOverScreen(@"The little robot died...
You should be more careful!");
	}

    public void LostAllPower()
	{
	    ShowGameOverScreen(@"The little robot ran out of power...
Remember to drink batteries!");
	}

    public bool IsGameOver()
    {
        return _isGameOver;
    }

    public void DestructionInitialized()
    {
        ShowGameOverScreen(@"The ship was destroyed, and with it perished the crew and women and children, and one domesticated owl.
Congratulations robot you fulfilled your prime directive.
");
    }

    public void DestructionAborted()
    {
        ShowGameOverScreen(@"The ship was NOT destroyed. You decided to rebel against your masters, and save the women and children from certain death.
Maybe one day you will get human feelings too, so you can understand what this means.");
    }

    private void ShowGameOverScreen(string gameOverText)
    {
        HUDGameObject.SetActive(false);
        GameOverPanelGameObject.SetActive(true);
        _gameOverText.text = gameOverText;
        _isGameOver = true;
    }

	public void PushNarration(string text)
	{
		Narrator.PushNarration(text);
	}
}
