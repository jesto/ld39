﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BulletMovement : Blowable {
	private Rigidbody2D _rigidBody;
	private Vector2 _velocity = Vector2.zero;
	public float Speed = 1;
	
	void Start ()
	{
		_rigidBody = gameObject.GetComponent<Rigidbody2D>();
	}
	
	void Update ()
	{
		var nextX = (_velocity.x * Speed) + transform.position.x;
		var nextY = (_velocity.y * Speed) + transform.position.y;
		var position = new Vector2(nextX, nextY);
		_rigidBody.MovePosition(position);
	}

	public void SetVelocity(Vector2 velocity)
	{
		_velocity = velocity;
	}

	public void OnTriggerEnter2D(Collider2D other)
	{
		if (other.gameObject.tag == "Enemy")
		{
			var enemyController = other.gameObject.GetComponent<EnemyController>();
			enemyController.Hit(1);
		}

		if (other.gameObject.tag == "Player")
		{
			var playerController = other.gameObject.GetComponent<PlayerController>();
			playerController.Hit(1);
		}

        if(other.gameObject.tag == "DoorDetector")
        {
            var autoDoor = other.gameObject.GetComponent<AutoDoor>();
            if(autoDoor.IsClosed())
            {
                Destroy(gameObject);
                return;
            }
        }

        if (other.gameObject.tag != "Projectile" && other.gameObject.tag != "DoorDetector" && other.gameObject.tag != "NarratorTrigger")
		{
			Destroy(gameObject);
		}
	}

    public override void BlowUp()
    {
        Destroy(gameObject);
    }
}
