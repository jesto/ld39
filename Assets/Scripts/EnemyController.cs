﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyController : Blowable
{
	public int Health = 0;
	public float MovementSpeed = 1;
	private Rigidbody2D _rigidbody2D;
	public float ShootDelay = 0;
	private float _timeSinceLastShot = 0;
	public int Damage = 1;
	private GameObject _playerGameObject;
    public float ActivationRange;
    public GameObject GameManager;
    private GameManager _gameManager;
    private Animator _animator;
    public int DmgTakenFromExplosions = 100;
    public GameObject SpawnObjectOnDeath;
    
    // Use this for initialization
    void Start ()
	{
		_rigidbody2D = GetComponent<Rigidbody2D>();
		_playerGameObject = GameObject.FindWithTag("Player");
	    _gameManager = GameManager.GetComponent<GameManager>();
	    _animator = GetComponent<Animator>();
    }

    // Update is called once per frame
    void FixedUpdate () {
        if(_gameManager.IsGameOver())
        {
            return;
        }

        var position = transform.position;

		if (_playerGameObject == null)
		{
			Debug.Log("Player Game Object can not be found");
			_rigidbody2D.velocity = Vector2.zero;
			return;
		}

		var target = _playerGameObject.transform.position;

	    if (Vector2.Distance(position, target) > ActivationRange)
	    {
            return;
        }

        var horizontal = 0;
		if (target.x < position.x)
			horizontal = -1;
		else if(target.x > position.x)
			horizontal = 1;

		var vertical = 0;
		if (target.y < position.y)
			vertical = -1;
		else if (target.y > position.y)
            vertical = 1;

		Move(horizontal, vertical);

        _animator.SetBool("MovingLeft", horizontal < 0);
        _animator.SetBool("MovingRight", horizontal > 0);
        _animator.SetBool("MovingDown", vertical < 0);
        _animator.SetBool("MovingUp", vertical > 0);
    }

	public void OnCollisionEnter2D(Collision2D other)
	{
		TryToHitPlayer(other);
	}

	public void OnCollisionStay2D(Collision2D other)
	{
		_timeSinceLastShot += Time.deltaTime;

		if (_timeSinceLastShot <= ShootDelay)
			return;

		TryToHitPlayer(other);

		_timeSinceLastShot = 0;	
	}

	private void TryToHitPlayer(Collision2D other)
	{
		if (other.gameObject.tag == "Player")
		{
			var playerController = other.gameObject.GetComponent<PlayerController>();
			playerController.Hit(Damage);
		}
	}

	private void Move(int horizontal, int vertical)
	{
		_rigidbody2D.velocity = new Vector2(horizontal * MovementSpeed, vertical * MovementSpeed);
	}

	public void Hit(int dmg)
	{
		Health -= dmg;

		if (Health <= 0)
		{
            if(SpawnObjectOnDeath != null)
            {
                Instantiate(SpawnObjectOnDeath, transform.position, Quaternion.identity);
            }
            Destroy(gameObject);
		}
	}

    public override void BlowUp()
    {
        Hit(DmgTakenFromExplosions);
    }
}
