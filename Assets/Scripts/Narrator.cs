﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Narrator : MonoBehaviour {
	private Text _textField;
	private IList<string> _lines = new List<string>();
	private int _maxLines = 22;
	private int _totalLines = 0;

	// Use this for initialization
	void Start ()
	{
		_textField = GetComponentInChildren<Text>();
	}

    public void PushNarration(string text)
    {
	    if (_textField == null)
	    {
		    _textField = GetComponentInChildren<Text>();
				return;
			}

        _totalLines++;

        _lines.Add(text);

        TruncateLines();

        var fullText = "";
        var startLine = Mathf.Max(0, _totalLines - _maxLines);

        for (var i = 0; i < _lines.Count; i++)
        {
            fullText += string.Format("[{0:00}] {1}\n", startLine + i + 1, _lines[i]);
        }
        _textField.text = fullText;
    }

    private void TruncateLines()
	{
		if (_lines.Count > _maxLines)
		{
			_lines.RemoveAt(0);
			TruncateLines();
		}
	}
}
